﻿using System.Collections.Generic;

namespace MongoEntities
{
    [BsonCollection("Categories")]
    public class Category : Document
    {
        public string Name { get; set; }

        public int CategoryId { get; set; }
    }
}
