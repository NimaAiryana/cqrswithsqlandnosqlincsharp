using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace MongoEntities
{
    public interface IDocument
    {
        [BsonId(IdGenerator = typeof(StringObjectIdGenerator))]
        string Id { get; set; }

        DateTimeOffset CreatedAtUtc { get; }
    }
}