namespace MongoEntities
{
    [BsonCollection("Products")]
    public class Product : Document
    {
        public string Name { get; set; }

        public int ProductId { get; set; }

        public int CategoryId { get; set; }

        public Category Category { get; set; }
    }
}