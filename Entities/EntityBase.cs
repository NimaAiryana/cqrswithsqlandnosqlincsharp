using System;

namespace Entities
{
    public abstract class EntityBase<TKey>
    {
        public TKey Id { get; set; }

        public DateTimeOffset CreatedAt { get; set; } = DateTimeOffset.UtcNow;
    }
}