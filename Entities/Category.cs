﻿using System.Collections.Generic;

namespace Entities
{
    public class Category : EntityBase<int>, IEntity
    {
        public string Name { get; set; }
    }
}
