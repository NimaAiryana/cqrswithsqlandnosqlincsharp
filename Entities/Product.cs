namespace Entities
{
    public class Product : EntityBase<int>, IEntity
    {
        public int CategoryId { get; set; }

        public string Name { get; set; }
    }
}