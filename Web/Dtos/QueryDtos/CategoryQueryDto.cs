using System;
using System.ComponentModel.DataAnnotations;
using MediatR;
namespace Web.Dtos.CommandDtos
{
    public sealed class CategoryQueryDto : IRequest<Result<CategoryQueryDto>>, IDto
    {
        public CategoryQueryDto() { }

        public CategoryQueryDto(string id) => (Id) = (id);

        public string Id { get; set; }

        public string Name { get; set; }

        public int CategoryId { get; set; }

        public DateTimeOffset CreatedAtUtc { get; set; }
    }
}