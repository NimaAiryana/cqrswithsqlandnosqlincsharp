using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MediatR;
namespace Web.Dtos.CommandDtos
{
    public sealed class ProductsQueryDto : IRequest<Result<IList<ProductsQueryDto>>>, IDto
    {
        public string Id { get; set; }

        public DateTimeOffset CreatedAtUtc { get; set; }

        public string Name { get; set; }

        public int ProductId { get; set; }

        public int CategoryId { get; set; }
        
        public MongoEntities.Category Category { get; set; }
    }
}