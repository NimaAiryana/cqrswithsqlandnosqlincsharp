using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MediatR;
namespace Web.Dtos.CommandDtos
{
    public sealed class CategoriesQueryDto : IRequest<Result<IList<CategoriesQueryDto>>>, IDto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public int CategoryId { get; set; }

        public DateTimeOffset CreatedAtUtc { get; set; }
    }
}