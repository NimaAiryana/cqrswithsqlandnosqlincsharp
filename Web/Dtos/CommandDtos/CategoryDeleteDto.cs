using MediatR;

namespace Web.Dtos.CommandDtos
{
    public sealed class CategoryDeleteDto : IRequest<Result<CategoryDeleteDto>>
    {
        public CategoryDeleteDto() { }

        public CategoryDeleteDto(string id) => Id = id;
        
        public string Id { get; set; }
    }
}