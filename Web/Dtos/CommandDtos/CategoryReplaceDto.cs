using MediatR;

namespace Web.Dtos.CommandDtos
{
    public sealed class CategoryReplaceDto : IRequest<Result<CategoryReplaceDto>>
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}