using System.ComponentModel.DataAnnotations;
using MediatR;
namespace Web.Dtos.CommandDtos
{
    public sealed class CategoryCommandDto : IRequest<Result<CategoryCommandDto>>, IDto
    {
        [Required]
        public string Name { get; set; }
    }
}