using MediatR;

namespace Web.Dtos.CommandDtos
{
    public sealed class ProductDeleteDto : IRequest<Result<ProductDeleteDto>>
    {
        public ProductDeleteDto() { }

        public ProductDeleteDto(string id) => Id = id;
        
        public string Id { get; set; }
    }
}