using MediatR;

namespace Web.Dtos.CommandDtos
{
    public sealed class ProductReplaceDto : IRequest<Result<ProductReplaceDto>>
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}