using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Web.Dtos.CommandDtos;
using MapsterMapper;
using Mapster;
using Entities;
using Data;
using System.Collections.Generic;
using System.Linq;

namespace Web.CommandHandlers
{
    public class ProductsQueryHandler : IRequestHandler<ProductsQueryDto, Result<IList<ProductsQueryDto>>>
    {
        private readonly IMongoRepository<MongoEntities.Product> productMongoRepository;

        public ProductsQueryHandler(IMongoRepository<MongoEntities.Product> productMongoRepository) => this.productMongoRepository = productMongoRepository;

        public async Task<Result<IList<ProductsQueryDto>>> Handle(ProductsQueryDto request, CancellationToken cancellationToken) => 
            new Result<IList<ProductsQueryDto>>(true, new Mapper().Map<IList<ProductsQueryDto>>(await productMongoRepository.FilterByAsync(record => true)));
    }
}