using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Web.Dtos.CommandDtos;
using MapsterMapper;
using Mapster;
using Entities;
using Data;
using System.Collections.Generic;
using System.Linq;

namespace Web.CommandHandlers
{
    public class CategoriesQueryHandler : IRequestHandler<CategoriesQueryDto, Result<IList<CategoriesQueryDto>>>
    {
        private readonly IMongoRepository<MongoEntities.Category> categoryMongoRepository;

        public CategoriesQueryHandler(IMongoRepository<MongoEntities.Category> categoryMongoRepository) => this.categoryMongoRepository = categoryMongoRepository;

        public async Task<Result<IList<CategoriesQueryDto>>> Handle(CategoriesQueryDto request, CancellationToken cancellationToken) => 
            new Result<IList<CategoriesQueryDto>>(true, new Mapper().Map<IList<CategoriesQueryDto>>(await categoryMongoRepository.FilterByAsync(record => true)));
    }
}