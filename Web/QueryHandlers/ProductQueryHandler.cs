using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Web.Dtos.CommandDtos;
using MapsterMapper;
using Entities;
using Data;

namespace Web.CommandHandlers
{
    public class ProductQueryHandler : IRequestHandler<ProductQueryDto, Result<ProductQueryDto>>
    {
        private readonly IMongoRepository<MongoEntities.Product> productMongoRepository;

        public ProductQueryHandler(IMongoRepository<MongoEntities.Product> productMongoRepository) => this.productMongoRepository = productMongoRepository;

        public async Task<Result<ProductQueryDto>> Handle(ProductQueryDto request, CancellationToken cancellationToken) =>
            new Result<ProductQueryDto>(true, new Mapper().Map<ProductQueryDto>(await productMongoRepository.FindByIdAsync(request.Id)));
    }
}