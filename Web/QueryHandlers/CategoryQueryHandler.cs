using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Web.Dtos.CommandDtos;
using MapsterMapper;
using Entities;
using Data;

namespace Web.CommandHandlers
{
    public class CategoryQueryHandler : IRequestHandler<CategoryQueryDto, Result<CategoryQueryDto>>
    {
        private readonly IMongoRepository<MongoEntities.Category> categoryMongoRepository;

        public CategoryQueryHandler(IMongoRepository<MongoEntities.Category> categoryMongoRepository) => this.categoryMongoRepository = categoryMongoRepository;

        public async Task<Result<CategoryQueryDto>> Handle(CategoryQueryDto request, CancellationToken cancellationToken) =>
            new Result<CategoryQueryDto>(true, new Mapper().Map<CategoryQueryDto>(await categoryMongoRepository.FindByIdAsync(request.Id)));
    }
}