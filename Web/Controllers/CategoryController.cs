using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.Dtos.CommandDtos;

namespace Web.Controllers
{
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly IMediator mediator;

        public CategoryController(IMediator mediator) => this.mediator = mediator;

        [HttpPost]
        [Route("/api/category")]
        public async Task<IActionResult> InsertCategoryAsync([FromBody] CategoryCommandDto categoryDto) => Ok(await mediator.Send(categoryDto));

        [Route("/api/category")]
        public async Task<IActionResult> GetCategoryAsync(string id) => Ok(await mediator.Send(new CategoryQueryDto(id)));

        [Route("/api/categories")]
        public async Task<IActionResult> GetCategoriesAsync() => Ok(await mediator.Send(new CategoriesQueryDto()));

        [HttpDelete]
        [Route("/api/category")]
        public async Task<IActionResult> DeleteAsync(string id) => Ok(await mediator.Send(new CategoryDeleteDto(id)));

        [HttpPut]
        [Route("/api/category")]
        public async Task<IActionResult> ReplaceAsync([FromBody] CategoryReplaceDto request) => Ok(await mediator.Send(request));
    }
}