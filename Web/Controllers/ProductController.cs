using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Web.Dtos.CommandDtos;

namespace Web.Controllers
{
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMediator mediator;

        public ProductController(IMediator mediator) => this.mediator = mediator;

        [HttpPost]
        [Route("/api/product")]
        public async Task<IActionResult> InsertAsync([FromBody] ProductInsertDto productDto) => Ok(await mediator.Send(productDto));

        [Route("/api/products")]
        public async Task<IActionResult> GetsAsync() => Ok(await mediator.Send(new ProductsQueryDto()));

        [Route("/api/product")]
        public async Task<IActionResult> GetAsync(string id) => Ok(await mediator.Send(new ProductQueryDto(id)));

        [HttpDelete]
        [Route("/api/product")]
        public async Task<IActionResult> DeleteAsync(string id) => Ok(await mediator.Send(new ProductDeleteDto(id)));

        [HttpPut]
        [Route("/api/product")]
        public async Task<IActionResult> ReplaceAsync([FromBody] ProductReplaceDto request) => Ok(await mediator.Send(request));
    }
}