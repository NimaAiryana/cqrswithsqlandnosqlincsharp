using MediatR;

namespace Web
{
    public sealed class Result<TResponse> where TResponse : class
    {
        public Result(bool succeeded, TResponse data) => (Succeeded, Data) = (succeeded, data);

        public bool Succeeded { get; set; }

        public TResponse Data { get; set; }
    }
}