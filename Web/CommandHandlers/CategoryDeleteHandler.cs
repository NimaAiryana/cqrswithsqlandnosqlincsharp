using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data;
using Entities;
using MediatR;
using Web.Dtos.CommandDtos;

namespace Web.CommandHandlers
{
    public sealed class CategoryDeleteHandler : IRequestHandler<CategoryDeleteDto, Result<CategoryDeleteDto>>
    {
        private readonly IRepository<Category> categoryRepository;
        private readonly IMongoRepository<MongoEntities.Category> categoryMongoRepository1;
        private readonly IRepository<Product> productRepository;
        private readonly IMongoRepository<MongoEntities.Product> productMongoRepository;
        private readonly IMongoRepository<MongoEntities.Category> categoryMongoRepository;

        public CategoryDeleteHandler(IRepository<Category> categoryRepository,
            IMongoRepository<MongoEntities.Category> categoryMongoRepository,
            IRepository<Product> productRepository,
            IMongoRepository<MongoEntities.Product> productMongoRepository)
        {
            this.categoryRepository = categoryRepository;
            this.categoryMongoRepository = categoryMongoRepository;
            this.productRepository = productRepository;
            this.productMongoRepository = productMongoRepository;
        }

        public async Task<Result<CategoryDeleteDto>> Handle(CategoryDeleteDto request, CancellationToken cancellationToken)
        {
            var nosql = await categoryMongoRepository.FindByIdAsync(request.Id);

            if (nosql is null) throw new NullReferenceException($"category {request.Id} is null, check you product");

            var sql = await categoryRepository.GetByIdAsync(cancellationToken, nosql.CategoryId);

            if (sql is null) throw new NullReferenceException($"category {request.Id} is null, check you product in sql");

            await categoryRepository.DeleteAsync(sql, cancellationToken);

            await categoryMongoRepository.DeleteByIdAsync(nosql.Id);

            // delete all cat pros in sql

            await productRepository.DeleteRangeAsync(productRepository.Table.Where(record => record.CategoryId == sql.Id).ToList(), cancellationToken);

            // delete all cat pros in nosql

            await productMongoRepository.DeleteManyAsync(record => record.CategoryId == sql.Id);
            
            return new Result<CategoryDeleteDto>(true, null);
        }
    }
}