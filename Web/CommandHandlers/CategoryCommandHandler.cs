using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Web.Dtos.CommandDtos;
using MapsterMapper;
using Entities;
using Data;

namespace Web.CommandHandlers
{
    public class CategoryCommandHandler : IRequestHandler<CategoryCommandDto, Result<CategoryCommandDto>>
    {
        private readonly IRepository<Category> categoryRepository;
        private readonly IMongoRepository<MongoEntities.Category> categoryMongoRepository;

        public CategoryCommandHandler(IRepository<Category> categoryRepository,
            IMongoRepository<MongoEntities.Category> categoryMongoRepository)
        {
            this.categoryRepository = categoryRepository;
            this.categoryMongoRepository = categoryMongoRepository;
        }

        public async Task<Result<CategoryCommandDto>> Handle(CategoryCommandDto request, CancellationToken cancellationToken)
        {
            var sqlCategory = new Mapper().Map<Category>(request);

            await categoryRepository.AddAsync(sqlCategory, cancellationToken);

            var nosqlCategory = new Mapper().Map<MongoEntities.Category>(request);

            nosqlCategory.CategoryId = sqlCategory.Id;

            await categoryMongoRepository.InsertOneAsync(nosqlCategory);

            return new Result<CategoryCommandDto>(true, null);
        }
    }
}