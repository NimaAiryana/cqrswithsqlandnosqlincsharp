using System;
using System.Threading;
using System.Threading.Tasks;
using Data;
using Entities;
using MediatR;
using Web.Dtos.CommandDtos;

namespace Web.CommandHandlers
{
    public sealed class ProductReplaceHandler : IRequestHandler<ProductReplaceDto, Result<ProductReplaceDto>>
    {
        private readonly IRepository<Product> productRepository;
        private readonly IMongoRepository<MongoEntities.Product> productMongoRepository;

        public ProductReplaceHandler(IRepository<Product> productRepository,
            IMongoRepository<MongoEntities.Product> productMongoRepository)
        {
            this.productRepository = productRepository;
            this.productMongoRepository = productMongoRepository;
        }

        public async Task<Result<ProductReplaceDto>> Handle(ProductReplaceDto request, CancellationToken cancellationToken)
        {
            var nosql = await productMongoRepository.FindByIdAsync(request.Id);

            if (nosql is null) throw new NullReferenceException($"product {request.Id} is null, check you product");

            var sql = await productRepository.GetByIdAsync(cancellationToken, nosql.ProductId);

            if (sql is null) throw new NullReferenceException($"product {request.Id} is null in sql, check you product");

            nosql.Name = request.Name;

            sql.Name = request.Name;

            await productMongoRepository.ReplaceOneAsync(nosql);

            await productRepository.UpdateAsync(sql, cancellationToken);

            return new Result<ProductReplaceDto>(true, null);
        }
    }
}