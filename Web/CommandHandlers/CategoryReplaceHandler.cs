using System;
using System.Threading;
using System.Threading.Tasks;
using Data;
using Entities;
using MediatR;
using Web.Dtos.CommandDtos;

namespace Web.CommandHandlers
{
    public sealed class CategoryReplaceHandler : IRequestHandler<CategoryReplaceDto, Result<CategoryReplaceDto>>
    {
        private readonly IRepository<Category> categoryRepository;
        private readonly IMongoRepository<MongoEntities.Category> categoryMongoRepository;
        private readonly IMongoRepository<MongoEntities.Product> productMongoRepository;

        public CategoryReplaceHandler(IRepository<Category> categoryRepository,
            IMongoRepository<MongoEntities.Category> categoryMongoRepository,
            IMongoRepository<MongoEntities.Product> productMongoRepository)
        {
            this.categoryRepository = categoryRepository;
            this.categoryMongoRepository = categoryMongoRepository;
            this.productMongoRepository = productMongoRepository;
        }

        public async Task<Result<CategoryReplaceDto>> Handle(CategoryReplaceDto request, CancellationToken cancellationToken)
        {
            var nosql = await categoryMongoRepository.FindByIdAsync(request.Id);

            if (nosql is null) throw new NullReferenceException($"category {request.Id} is null, check you category");

            var sql = await categoryRepository.GetByIdAsync(cancellationToken, nosql.CategoryId);

            if (sql is null) throw new NullReferenceException($"category {request.Id} is null in sql, check you category");

            nosql.Name = request.Name;

            sql.Name = request.Name;

            await categoryMongoRepository.ReplaceOneAsync(nosql);

            await categoryRepository.UpdateAsync(sql, cancellationToken);

            // change all cat products in nosql

            var allCatPros = await productMongoRepository.FilterByAsync(record => record.CategoryId == sql.Id);

            foreach (var product in allCatPros)
            {
                product.Category = nosql;

                await productMongoRepository.ReplaceOneAsync(product);
            }

            return new Result<CategoryReplaceDto>(true, null);
        }
    }
}