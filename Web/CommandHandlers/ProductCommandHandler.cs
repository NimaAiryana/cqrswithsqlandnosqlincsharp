using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Web.Dtos.CommandDtos;
using MapsterMapper;
using Entities;
using Data;

namespace Web.CommandHandlers
{
    public class ProductCommandHandler : IRequestHandler<ProductInsertDto, Result<ProductInsertDto>>
    {
        private readonly IRepository<Product> productRepository;
        private readonly IRepository<Category> categoryRepository;
        private readonly IMongoRepository<MongoEntities.Product> productMongoRepository;

        public ProductCommandHandler(IRepository<Product> productRepository,
            IRepository<Category> categoryRepository,
            IMongoRepository<MongoEntities.Product> productMongoRepository)
        {
            this.productRepository = productRepository;
            this.categoryRepository = categoryRepository;
            this.productMongoRepository = productMongoRepository;
        }

        public async Task<Result<ProductInsertDto>> Handle(ProductInsertDto request, CancellationToken cancellationToken)
        {
            var sqlProduct = new Mapper().Map<Product>(request);

            await productRepository.AddAsync(sqlProduct, cancellationToken);

            var mongoProductInstance = new Mapper().Map<MongoEntities.Product>(request);

            var sqlCategory = await categoryRepository.GetByIdAsync(cancellationToken, request.CategoryId);

            mongoProductInstance.Category = new Mapper().Map<MongoEntities.Category>(sqlCategory);

            mongoProductInstance.ProductId = sqlProduct.Id;

            await productMongoRepository.InsertOneAsync(mongoProductInstance);

            return new Result<ProductInsertDto>(true, null);
        }
    }
}