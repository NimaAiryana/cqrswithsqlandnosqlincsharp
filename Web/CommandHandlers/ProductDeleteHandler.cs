using System;
using System.Threading;
using System.Threading.Tasks;
using Data;
using Entities;
using MediatR;
using Web.Dtos.CommandDtos;

namespace Web.CommandHandlers
{
    public sealed class ProductDeleteHandler : IRequestHandler<ProductDeleteDto, Result<ProductDeleteDto>>
    {
        private readonly IRepository<Product> productRepository;
        private readonly IMongoRepository<MongoEntities.Product> productMongoRepository;

        public ProductDeleteHandler(IRepository<Product> productRepository,
            IMongoRepository<MongoEntities.Product> productMongoRepository)
        {
            this.productRepository = productRepository;
            this.productMongoRepository = productMongoRepository;
        }

        public async Task<Result<ProductDeleteDto>> Handle(ProductDeleteDto request, CancellationToken cancellationToken)
        {
            var nosqlProduct = await productMongoRepository.FindByIdAsync(request.Id);

            if (nosqlProduct is null) throw new NullReferenceException($"product {request.Id} is null, check you product");

            var sqlProduct = await productRepository.GetByIdAsync(cancellationToken, nosqlProduct.ProductId);

            if (sqlProduct is null) throw new NullReferenceException($"product {request.Id} is null, check you product in sql");

            await productRepository.DeleteAsync(sqlProduct, cancellationToken);

            await productMongoRepository.DeleteByIdAsync(nosqlProduct.Id);

            return new Result<ProductDeleteDto>(true, null);
        }
    }
}